import {createRestClient} from "scarlett";

export const useRestClient = createRestClient({
	host: window.location.toString(),
	responseType: "json",
});