import React, {ReactElement, useEffect, useMemo, useState} from 'react';
import AppLayout from './containers/AppLayout';
import StatusPage from './containers/pages/StatusPage';
import {createTheme, Stack, ThemeProvider, useMediaQuery} from "@mui/material";
import NavBar from "./containers/NavBar.tsx";
import {useRestClient} from "./config/client.ts";
import {Configuration} from "./types/Configuration.ts";
import {RestError} from "scarlett";

const App: React.FC = (): ReactElement => {
	const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");
	const {get} = useRestClient();

	const [data, setData] = useState<Configuration | null>(null);
	const [, setLoading] = useState<boolean>(false);
	const [, setError] = useState<RestError | null>(null);

	useEffect(() => {
		setError(() => null);
		setLoading(() => true);

		async function load() {
			const {data, error} = await get<Configuration>("/config.json");
			if (error != null) {
				setError(() => error);
				return;
			}
			setData(() => data);
		}
		load().then(() => setLoading(() => false));
	}, [get]);

	const theme = useMemo(() => createTheme({
		palette: {
			mode: prefersDarkMode ? "dark" : "light"
		},
		components: {
			MuiTooltip: {
				styleOverrides: {
					tooltip: ({theme}) => ({
						backgroundColor: theme.palette.background.paper,
						color: theme.palette.text.primary,
						border: `1px solid ${theme.palette.divider}`
					})
				}
			}
		}
	}), [prefersDarkMode]);

	return <ThemeProvider theme={theme}>
		<Stack>
			<AppLayout>
				<React.Fragment>
					<NavBar config={data?.["status-website"]}/>
					<StatusPage/>
				</React.Fragment>
			</AppLayout>
		</Stack>
	</ThemeProvider>
}
export default App;
