import React, {ReactElement} from "react";
import {Box, Button, Stack, Typography} from "@mui/material";
import {WebConfig} from "../types/Configuration.ts";
import {Link} from "react-router-dom";
import {mdiOpenInNew} from "@mdi/js";
import Icon from "@mdi/react";
import MuiMarkdown from "mui-markdown";

interface Props {
	config?: WebConfig;
}

const NavBar: React.FC<Props> = ({config}): ReactElement => {
	return <Stack>
		{config?.logoUrl && <img
            style={{marginRight: 16, objectFit: "contain"}}
            alt={"Page logo"}
            src={config?.logoUrl}
            height={64}
        />}
		<Box
			sx={{display: "flex"}}>
			<Typography
				variant={"h2"}>
				{config?.name || "Uptime"}
			</Typography>
			<Box sx={{flexGrow: 1}}/>
			{(config?.navbar ?? []).map(i => <Button
				variant={"outlined"}
				component={Link}
				to={i.href}
				target={i.href.startsWith("https://") ? "_blank" : undefined}
				sx={{borderRadius: 2, m: 2, mr: 1, ml: 1}}
				key={i.title}
				endIcon={i.href.startsWith("https://") ? <Icon path={mdiOpenInNew} size={0.75}/> : undefined}>
				{i.title}
			</Button>)}
		</Box>
		<Box sx={{height: "2px", backgroundColor: "divider", opacity: 0.34, mb: 2, mt: 2}}/>
		{(config?.introTitle || config?.introMessage) && <Stack>
			{config?.introTitle && <Box
				sx={{mb: 1}}>
                <MuiMarkdown>
					{config.introTitle}
                </MuiMarkdown>
            </Box>}
			{config?.introMessage && <MuiMarkdown>
				{config.introMessage}
            </MuiMarkdown>}
            <Box sx={{height: "2px", backgroundColor: "divider", opacity: 0.34, mb: 2, mt: 2}}/>
        </Stack>}
	</Stack>
}
export default NavBar;