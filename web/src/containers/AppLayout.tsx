import React, {ReactElement} from "react";
import {Grid} from "@mui/material";

interface Props {
	children: ReactElement;
}

const AppLayout: React.FC<Props> = ({children}): ReactElement => {
	return <Grid container sx={{width: "100vw", height: "calc(100vh - 74px)"}}>
		<Grid item xs={1} md={2} xl={4}/>
		<Grid item xs={10} md={8} xl={4}>
			{children}
		</Grid>
		<Grid item xs={1} md={2} xl={4}/>
	</Grid>
}
export default AppLayout;
