import React, {ReactElement} from "react";
import {Box, IconButton, ListSubheader, Stack, Typography} from "@mui/material";
import {Site} from "../../types/Status.ts";
import {RestError} from "scarlett";
import IncidentHistory from "./IncidentHistory.tsx";
import {Link} from "react-router-dom";
import Icon from "@mdi/react";
import {mdiArrowLeft} from "@mdi/js";

interface Props {
	data: Site[];
	error?: RestError | null;
	loading?: boolean;
}

const HistoryPage: React.FC<Props> = ({data, loading, error}): ReactElement => {
	return <Stack>
		<ListSubheader
			disableGutters
			disableSticky>
			<IconButton
				component={Link}
				to={"/"}
				size={"small"}
				sx={{mr: 1}}>
				<Icon path={mdiArrowLeft} size={0.75}/>
			</IconButton>
			Back to home
		</ListSubheader>
		<Box sx={{display: "flex", width: "100%"}}>
			<Typography>
				Incident History
			</Typography>
		</Box>
		<IncidentHistory
			data={data}
			error={error}
			loading={loading}
			limit={90}
		/>
	</Stack>
}
export default HistoryPage;
