import React, {ReactElement, useMemo, useState} from "react";
import {Box, Card, Collapse, IconButton, ListItem, ListItemAvatar, ListItemText, Tooltip} from "@mui/material";
import StatusTimeline from "./StatusTimeline.tsx";
import {Site} from "../../../types/Status.ts";
import {endOfToday, isSameDay, subDays} from "date-fns";
import {TimelineItem} from "../LiveStatus.tsx";
import {Incident} from "../../../types/Incidents.ts";
import Icon from "@mdi/react";
import {mdiChevronRight} from "@mdi/js";
import StatusComponent from "./StatusComponent.tsx";

interface Props {
	data: Site[];
	component: string;
}

const StatusComponentParent: React.FC<Props> = ({data, component}): ReactElement => {
	const [expanded, setExpanded] = useState<boolean>(false);

	const timeline = useMemo(() => {
		const end = endOfToday();
		const items: TimelineItem[] = [];
		for (let i = 0; i < 90; i++) {
			const date = subDays(end, i);
			const initialData: Incident[] = [];
			const matchingIncidents = data.reduce((acc, current) => {
				const matches = current.issues.filter(d => isSameDay(date, d.created_at) || isSameDay(date, d.closed_at)) || [];
				return acc.concat(matches);
			}, initialData);
			items.push({
				date: date.toDateString(),
				incidents: matchingIncidents
			});
		}
		return items;
	}, [data]);

	return <Box>
		<ListItem>
			<ListItemAvatar
				sx={{minWidth: 48}}>
				<Tooltip title={expanded ? "Hide components" : "Show components"}>
					<IconButton
						centerRipple={false}
						onClick={() => setExpanded(e => !e)}>
						<Icon
							path={mdiChevronRight}
							size={1}
							style={{transition: "transform 0.1s linear", transform: `rotate(${expanded ? "90" : "0"}deg)`}}
						/>
					</IconButton>
				</Tooltip>
			</ListItemAvatar>
			<ListItemText
				primary={component}
			/>
		</ListItem>
		<Collapse
			in={!expanded}>
			<StatusTimeline items={timeline}/>
		</Collapse>
		<Collapse
			in={expanded}
			sx={{ml: 1, mr: 1}}>
			<Card
				variant={"outlined"}
				sx={{pb: 2}}>
				{data.map(d => <StatusComponent
					key={d.name}
					site={d}
				/>)}
			</Card>
		</Collapse>
	</Box>
}
export default StatusComponentParent;
