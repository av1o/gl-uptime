import React, {ReactElement, useMemo} from "react";
import {Box, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, Typography} from "@mui/material";
import StatusTimeline from "./StatusTimeline.tsx";
import {Site} from "../../../types/Status.ts";
import {endOfToday, isSameDay, subDays} from "date-fns";
import {TimelineItem} from "../LiveStatus.tsx";
import Icon from "@mdi/react";
import {mdiWeb} from "@mdi/js";

interface Props {
	site: Site;
}

const StatusComponent: React.FC<Props> = ({site}): ReactElement => {
	const ok = site.issues.find(i => i.closed_at === 0) == null;

	const timeline = useMemo(() => {
		const end = endOfToday();
		const items: TimelineItem[] = [];
		for (let i = 0; i < 90; i++) {
			const date = subDays(end, i);
			const matchingIncidents = site.issues.filter(d => isSameDay(date, d.created_at) || isSameDay(date, d.closed_at)) || [];
			items.push({
				date: date.toDateString(),
				incidents: matchingIncidents
			});
		}
		return items;
	}, [site]);

	return <Box>
		<ListItem>
			<ListItemIcon
				sx={{minWidth: 40}}>
				{site.icon && <img
                    alt={`${site.name} icon`}
                    src={site.icon}
                    width={"auto"}
                    height={24}
                />}
				{!site.icon && <Icon path={mdiWeb} size={1}/>}
			</ListItemIcon>
			<ListItemText
				primary={site.name}
			/>
			<ListItemSecondaryAction>
				<Typography
					variant={"caption"}
					color={theme => ok ? theme.palette.success.main : theme.palette.error.main}>
					{ok ? "Operational" : "Degraded"}
				</Typography>
			</ListItemSecondaryAction>
		</ListItem>
		<StatusTimeline items={timeline}/>
	</Box>
}
export default StatusComponent;
