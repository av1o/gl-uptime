import React, {ReactElement} from "react";
import {Box, CardContent, Table, TableBody, TableCell, TableRow, Tooltip, Typography} from "@mui/material";
import {formatDistanceStrict} from "date-fns";
import {TimelineItem} from "./StatusTimeline.tsx";

interface Props {
	item: TimelineItem;
	children: ReactElement;
}

const StatusTimelineTooltip: React.FC<Props> = ({item, children}): ReactElement => {
	return <Tooltip
		key={item.date}
		leaveDelay={200}
		arrow
		title={<Box
			sx={{minWidth: 300}}>
			<CardContent>
				<Typography>
					{item.date}
				</Typography>
				{item.incidents.length === 0 && <Typography variant={"body2"}>
                    No incidents recorded for this day.
                </Typography>}
				{item.incidents.length > 0 && <Table sx={{width: "100%", mt: 2}}>
                    <TableBody>
						{item.incidents.map(i => <TableRow
							key={i.iid}
							hover>
							<TableCell align={"left"} size={"small"}>Outage</TableCell>
							<TableCell
								size={"small"}>{formatDistanceStrict(i.closed_at || new Date(), i.created_at)}</TableCell>
						</TableRow>)}
                    </TableBody>
                </Table>}
			</CardContent>
		</Box>}>
		{children}
	</Tooltip>
}
export default StatusTimelineTooltip;
