import React, {ReactElement} from "react";
import {Incident} from "../../../types/Incidents.ts";
import {Box} from "@mui/material";
import StatusTimelineTooltip from "./StatusTimelineTooltip.tsx";

export interface TimelineItem {
	date: string;
	incidents: Incident[];
}

interface Props {
	items: TimelineItem[];
}

const StatusTimeline: React.FC<Props> = ({items}): ReactElement => {
	return <Box sx={{display: "flex", justifyContent: "space-between", ml: 2, mr: 2}}>
		{items.map(v => <StatusTimelineTooltip
			key={v.date}
			item={v}>
			<Box sx={{
				height: 32,
				width: 4,
				backgroundColor: v.incidents.length === 0 ? "success.main" : "error.main",
				mr: "1px",
				borderRadius: 0.5
			}}/>
		</StatusTimelineTooltip>)}
	</Box>
}
export default StatusTimeline;
