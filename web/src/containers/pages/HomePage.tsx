import React, {ReactElement} from "react";
import {Box, ListSubheader, Stack, Typography} from "@mui/material";
import LiveStatus from "./LiveStatus.tsx";
import {Site} from "../../types/Status.ts";
import {RestError} from "scarlett";
import IncidentHistory from "./IncidentHistory.tsx";
import {Link} from "react-router-dom";

interface Props {
	data: Site[];
	error?: RestError | null;
	loading?: boolean;
}

const HomePage: React.FC<Props> = ({data, loading, error}): ReactElement => {
	return <Stack>
		<Box sx={{display: "flex", width: "100%"}}>
			<Typography>
				Live Status
			</Typography>
			<Box sx={{flexGrow: 1}}/>
			<Typography variant={"caption"}>
				Uptime over the last 90 days
			</Typography>
		</Box>
		<LiveStatus
			data={data}
			error={error}
			loading={loading}
		/>
		<Box sx={{display: "flex", width: "100%"}}>
			<Typography>
				Incident History
			</Typography>
		</Box>
		<IncidentHistory
			data={data}
			error={error}
			loading={loading}
		/>
		<ListSubheader
			disableSticky
			component={Link}
			to={"/history"}>
			View full history
		</ListSubheader>
	</Stack>
}
export default HomePage;
