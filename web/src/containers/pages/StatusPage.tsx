import React, {ReactElement, useEffect, useState} from "react";
import {useRestClient} from "../../config/client.ts";
import {Site} from "../../types/Status.ts";
import {RestError} from "scarlett";
import {Routes} from "react-router-dom";
import {Route} from "react-router";
import HomePage from "./HomePage.tsx";
import HistoryPage from "./HistoryPage.tsx";

const StatusPage: React.FC = (): ReactElement => {
	const {get} = useRestClient();

	const [sites, setSites] = useState<Site[]>([]);
	const [loading, setLoading] = useState<boolean>(false);
	const [error, setError] = useState<RestError | null>(null);

	useEffect(() => {
		setError(() => null);
		setLoading(() => true);

		async function load() {
			const {data, error} = await get<Site[]>("/timeline.json");
			if (error != null) {
				setError(() => error);
				return;
			}
			setSites(() => data ?? []);
		}

		load().then(() => setLoading(() => false));
	}, [get]);

	return <Routes>
		<Route
			path={"/"}
			element={<HomePage
				data={sites}
				loading={loading}
				error={error}
			/>}
		/>
		<Route
			path={"/history"}
			element={<HistoryPage
				data={sites}
				loading={loading}
				error={error}
			/>}
		/>
	</Routes>
}
export default StatusPage;
