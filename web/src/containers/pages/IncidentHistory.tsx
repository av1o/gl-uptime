import React, {ReactElement, useMemo} from "react";
import {Alert, Box, Card, Link, List, ListItem, ListItemText, Skeleton, Stack, Typography} from "@mui/material";
import {Site} from "../../types/Status.ts";
import {RestError} from "scarlett";
import {Incident} from "../../types/Incidents.ts";
import {endOfToday, format, isSameDay, subDays} from "date-fns";

interface Props {
	data: Site[];
	error?: RestError | null;
	loading?: boolean;
	limit?: number;
}

interface Item {
	date: string;
	incidents: Incident[];
}

const IncidentHistory: React.FC<Props> = ({limit = 14, data, error = null, loading = false}): ReactElement => {

	const loadingData = useMemo(() => {
		const items = [];
		for (let i = 0; i < 14; i++) {
			items.push(<ListItem
				key={`${i}`}>
				<Stack sx={{width: "100%"}}>
					<Skeleton width={"20%"} height={32}/>
					<Skeleton width={"100%"}/>
				</Stack>
			</ListItem>)
		}
		return items;
	}, []);

	const allData = useMemo(() => {
		const items: Incident[] = [];
		data.forEach(d => items.push(...d.issues));
		return items;
	}, [data]);

	const getTimeline = () => {
		const end = endOfToday();
		const items: Item[] = [];
		for (let i = 0; i < limit; i++) {
			const date = subDays(end, i);
			const matchingIncidents = allData.filter(d => isSameDay(date, d.created_at) || isSameDay(date, d.closed_at)) || [];
			items.push({
				date: date.toDateString(),
				incidents: matchingIncidents
			});
		}
		return items;
	}

	return <Card
		variant={"outlined"}
		sx={{mt: 1, mb: 1, p: 1}}>
		{error && <Alert severity={"error"}>
            Something went wrong.
        </Alert>}
		<List>
			{loading && loadingData}
			{getTimeline().map(d => {
				return <Box key={d.date}>
					<ListItem>
						<ListItemText
							primary={d.date}
							secondary={d.incidents.length === 0 ? "No incidents reported." : <Box>
								{d.incidents.map(i => <Box
									key={i.iid}>
									<Typography
										component={Link}
										href={i.web_url}
										target={"_blank"}>
										{i.title}
									</Typography>
									<Typography
										sx={{mt: 1}}
										variant={"body2"}>
										Reported at {format(i.created_at, "HH:mm:ss OOO")}<br/>
										{i.closed_at > 0 && `Resolved at ${format(i.closed_at, "HH:mm:ss OOO")}`}
									</Typography>
								</Box>)}
							</Box>}
						/>
					</ListItem>
				</Box>
			})}
		</List>
	</Card>
}
export default IncidentHistory;
