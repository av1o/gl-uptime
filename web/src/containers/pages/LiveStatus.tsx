import React, {ReactElement, useMemo} from "react";
import {Alert, Box, Card, List, ListItem, Skeleton, Stack} from "@mui/material";
import {Site} from "../../types/Status.ts";
import {RestError} from "scarlett";
import {Incident} from "../../types/Incidents.ts";
import StatusComponent from "./status/StatusComponent.tsx";
import StatusComponentParent from "./status/StatusComponentParent.tsx";

interface Props {
	data: Site[];
	error?: RestError | null;
	loading?: boolean;
}

export interface TimelineItem {
	date: string;
	incidents: Incident[];
}

const LiveStatus: React.FC<Props> = ({data, error = null, loading = false}): ReactElement => {

	const loadingData = useMemo(() => {
		const items = [];
		for (let i = 0; i < 6; i++) {
			items.push(<ListItem
				key={`${i}`}>
				<Stack sx={{width: "100%"}}>
					<Box sx={{display: "flex", width: "100%"}}>
						<Skeleton width={"20%"}/>
						<Box sx={{flexGrow: 1}}/>
						<Skeleton width={"10%"}/>
					</Box>
					<Skeleton width={"100%"} height={64}/>
				</Stack>
			</ListItem>)
		}
		return items;
	}, []);

	const components = useMemo(() => {
		return [...new Set(data.filter(d => d.component != null && d.component !== "").map(d => d.component!))];
	}, [data]);

	return <Card
		variant={"outlined"}
		sx={{mt: 1, mb: 1, p: 1}}>
		{error && <Alert severity={"error"}>
            Something went wrong.
        </Alert>}
		<List>
			{loading && loadingData}
			{components.map(c => <StatusComponentParent
				key={c}
				component={c}
				data={data.filter(d => d.component === c)}
			/>)}
			{data.filter(d => !d.component).map(d => <StatusComponent
				key={d.name}
				site={d}
			/>)}
		</List>
	</Card>
}
export default LiveStatus;
