import {Incident} from "./Incidents.ts";

export interface Site {
	name: string;
	url: string;
	issues: Incident[];
	component?: string;
	icon?: string;
}