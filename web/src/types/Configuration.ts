export interface NavItem {
	title: string;
	href: string;
}

export interface Configuration {
	"status-website": WebConfig;
}

export interface WebConfig {
	name?: string;
	logoUrl?: string;
	navbar?: NavItem[];
	introTitle?: string;
	introMessage?: string;
}