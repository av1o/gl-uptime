export interface Incident {
	iid: number;
	state: string;
	description: string;
	updated_at: number;
	closed_at: number;
	created_at: number;
	title: string;
	web_url: string;
}