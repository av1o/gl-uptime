package cfg

type Configuration struct {
	Owner         string        `json:"owner"`
	Repo          string        `json:"repo"`
	UserAgent     string        `json:"user-agent"`
	Sites         []Site        `json:"sites"`
	StatusWebsite StatusWebsite `json:"status-website" yaml:"status-website"`
}

type Site struct {
	Name                string   `json:"name"`
	URL                 string   `json:"url,omitempty"`
	Method              string   `json:"method,omitempty"`
	Headers             []string `json:"headers,omitempty"`
	Body                string   `json:"body,omitempty"`
	Icon                string   `json:"icon,omitempty"`
	ExpectedStatusCodes []int    `json:"expectedStatusCodes,omitempty"`
	MaxResponseTime     int      `json:"maxResponseTime,omitempty"`
	Insecure            bool     `json:"__dangerous__insecure,omitempty" yaml:"__dangerous__insecure,omitempty"`
	Component           string   `json:"component,omitempty"`
}

type StatusWebsite struct {
	Name         string    `json:"name,omitempty"`
	LogoURL      string    `json:"logoUrl,omitempty" yaml:"logoUrl,omitempty"`
	Navbar       []NavLink `json:"navbar,omitempty"`
	IntroTitle   string    `json:"introTitle,omitempty" yaml:"introTitle,omitempty"`
	IntroMessage string    `json:"introMessage,omitempty" yaml:"introMessage,omitempty"`
}

type NavLink struct {
	Title string `json:"title"`
	Href  string `json:"href"`
}
