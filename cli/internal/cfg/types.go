package cfg

import (
	"time"
)

type Response struct {
	Timeout    bool
	StatusCode int
	Duration   time.Duration
}

type SiteData struct {
	Site
	Issues []SiteIssue `json:"issues"`
}

type SiteIssue struct {
	IID         int    `json:"iid"`
	State       string `json:"state"`
	Description string `json:"description"`
	UpdatedAt   int64  `json:"updated_at"`
	ClosedAt    int64  `json:"closed_at"`
	CreatedAt   int64  `json:"created_at"`
	Title       string `json:"title"`
	WebURL      string `json:"web_url"`
}
