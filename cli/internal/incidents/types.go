package incidents

import (
	"github.com/djcass44/gl-uptime/cli/internal/cfg"
	"github.com/xanzy/go-gitlab"
)

type Service struct {
	pid string
	git *gitlab.Client
}

type Template struct {
	Site     *cfg.Site
	Response *cfg.Response
}

type IncidentAPI interface {
	IsOpen(site *cfg.Site) (bool, error)
	Open(site *cfg.Site, resp *cfg.Response) error
	Close(site *cfg.Site) error
}
