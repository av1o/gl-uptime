package incidents

import (
	"bytes"
	_ "embed"
	"fmt"
	"github.com/djcass44/gl-uptime/cli/internal/cfg"
	"github.com/xanzy/go-gitlab"
	"html/template"
	"log"
)

const (
	issueTypeIncident = "incident"
	issueStateOpened  = "opened"
	issueStateClosed  = "close"
)

//go:embed issue.tpl.md
var issueTemplateData string

func NewService(pid string, git *gitlab.Client) *Service {
	return &Service{
		pid: pid,
		git: git,
	}
}

func (svc *Service) List(site *cfg.Site, state *string) ([]*gitlab.Issue, error) {
	issues, _, err := svc.git.Issues.ListProjectIssues(svc.pid, &gitlab.ListProjectIssuesOptions{
		Search:    gitlab.String(issueName(site)),
		State:     state,
		IssueType: gitlab.String(issueTypeIncident),
	})
	if err != nil {
		return nil, fmt.Errorf("searching issues: %w", err)
	}
	return issues, nil
}

func (svc *Service) IsOpen(site *cfg.Site) (bool, error) {
	issues, err := svc.List(site, gitlab.String(issueStateOpened))
	if err != nil {
		return false, err
	}
	return len(issues) > 0, nil
}

func (svc *Service) Open(site *cfg.Site, resp *cfg.Response) error {
	// generate the issue description
	tmpl, err := template.New("").Parse(issueTemplateData)
	if err != nil {
		return err
	}
	var out bytes.Buffer
	if err := tmpl.Execute(&out, Template{Site: site, Response: resp}); err != nil {
		return err
	}
	// create the issue
	issue, _, err := svc.git.Issues.CreateIssue(svc.pid, &gitlab.CreateIssueOptions{
		Title:       gitlab.String(issueName(site)),
		Description: gitlab.String(out.String()),
		IssueType:   gitlab.String(issueTypeIncident),
	})
	if err != nil {
		return fmt.Errorf("opening issue: %w", err)
	}
	log.Printf("opened issue %d", issue.IID)
	return nil
}

func (svc *Service) Close(site *cfg.Site) error {
	matches, _, err := svc.git.Issues.ListProjectIssues(svc.pid, &gitlab.ListProjectIssuesOptions{
		Search:    gitlab.String(issueName(site)),
		State:     gitlab.String(issueStateOpened),
		IssueType: gitlab.String(issueTypeIncident),
	})
	if err != nil {
		return fmt.Errorf("searching issues: %w", err)
	}
	if len(matches) == 0 {
		log.Printf("issue is not open")
		return nil
	}
	for _, m := range matches {
		_, _, err := svc.git.Issues.UpdateIssue(svc.pid, m.IID, &gitlab.UpdateIssueOptions{
			StateEvent: gitlab.String(issueStateClosed),
		})
		if err != nil {
			log.Printf("error closing issue %d: %s", m.IID, err)
			continue
		}
		log.Printf("closed issue %d", m.IID)
	}
	return nil
}

func issueName(site *cfg.Site) string {
	return fmt.Sprintf("%s is down", site.Name)
}
