## Summary

{{ if .Response.Timeout }}
Monitoring probes could not reach the site in the allocated time.
{{ else }}
Monitoring probes received an HTTP {{ .Response.StatusCode }} in {{ .Response.Duration }}.
{{ end }}
