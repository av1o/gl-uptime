package probes

import (
	"context"
	"errors"
	"fmt"
	"github.com/djcass44/gl-uptime/cli/internal/cfg"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func HttpProbe(ctx context.Context, site *cfg.Site) (bool, cfg.Response, error) {
	log.Printf("checking site %s (%s)", site.Name, site.URL)
	// set the method
	method := site.Method
	if method == "" {
		method = http.MethodGet
	}

	timeout := time.Second * 30
	if site.MaxResponseTime > 0 {
		timeout = time.Duration(site.MaxResponseTime)
	}

	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	var req *http.Request
	var err error

	// set the body
	if site.Body != "" {
		req, err = http.NewRequestWithContext(ctx, method, os.ExpandEnv(site.URL), strings.NewReader(site.Body))
	} else {
		req, err = http.NewRequestWithContext(ctx, method, os.ExpandEnv(site.URL), nil)
	}
	if err != nil {
		return false, cfg.Response{}, fmt.Errorf("could not prepare request %s: %w", site.Name, err)
	}
	// add headers
	for _, h := range site.Headers {
		k, v, ok := strings.Cut(h, ": ")
		if !ok {
			return false, cfg.Response{}, fmt.Errorf("could not parse header %s", h)
		}
		req.Header.Add(k, v)
	}

	// execute the request
	start := time.Now()
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		if errors.Is(err, context.DeadlineExceeded) {
			log.Printf("request exceeded deadline of %s", timeout)
			return false, cfg.Response{Timeout: true}, nil
		}
		log.Printf("failed to execute request: %s", err)
		return false, cfg.Response{}, nil
	}

	log.Printf("completed request in %s with code: %d", time.Since(start), resp.StatusCode)
	r := cfg.Response{
		StatusCode: resp.StatusCode,
		Duration:   time.Since(start),
	}

	// if there are no expected status codes then
	// assume all 20x and 30x responses are okay
	if len(site.ExpectedStatusCodes) == 0 {
		if resp.StatusCode < 200 || resp.StatusCode > 399 {
			log.Printf("response code is not expected (expected: <200 || >399, actual: %d)", resp.StatusCode)
			return false, r, nil
		}
		return true, r, nil
	}
	// if the configuration contains a list of expected codes,
	// use them
	for _, c := range site.ExpectedStatusCodes {
		if c != resp.StatusCode {
			log.Printf("response code is not expected (expected: %+v, actual: %d)", site.ExpectedStatusCodes, resp.StatusCode)
			return false, r, nil
		}
	}

	return true, r, nil
}
