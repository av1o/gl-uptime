package probes_test

import (
	"context"
	"github.com/djcass44/gl-uptime/cli/internal/cfg"
	"github.com/djcass44/gl-uptime/cli/internal/probes"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHttpProbe(t *testing.T) {
	var cases = []struct {
		in  *cfg.Site
		ok  bool
		err bool
	}{
		{
			&cfg.Site{
				Name:                "always-works",
				URL:                 "https://google.com",
				ExpectedStatusCodes: []int{200},
			},
			true,
			false,
		},
		{
			&cfg.Site{
				Name: "always-fails",
				URL:  "https://google.com/404",
			},
			false,
			false,
		},
		{
			&cfg.Site{
				Name: "io-timeout",
				URL:  "https://localhost:1234",
			},
			false,
			false,
		},
		{
			&cfg.Site{
				Name:                "expected-codes",
				URL:                 "https://google.com/404",
				ExpectedStatusCodes: []int{404},
			},
			true,
			false,
		},
		{
			&cfg.Site{
				Name:            "instant-timeout",
				URL:             "https://google.com",
				MaxResponseTime: 1,
			},
			false,
			false,
		},
	}

	for _, tt := range cases {
		t.Run(tt.in.Name, func(t *testing.T) {
			ok, err := probes.HttpProbe(context.TODO(), tt.in)
			assert.EqualValues(t, tt.ok, ok)
			if tt.err {
				assert.Error(t, err)
				return
			}
			assert.NoError(t, err)
		})
	}
}
