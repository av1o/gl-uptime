package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/djcass44/gl-uptime/cli/internal/cfg"
	"github.com/djcass44/gl-uptime/cli/internal/incidents"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v3"
	"os"
	"time"
)

var collectCmd = &cobra.Command{
	Use:  "collect",
	RunE: collect,
}

const (
	flagTimelineOutput = "timeline-output"
	flagConfigOutput   = "config-output"
)

func init() {
	collectCmd.Flags().StringP(flagConfig, "c", ".upptimerc.yml", "path to configuration yaml")
	collectCmd.Flags().String(flagTimelineOutput, "timeline.json", "path to write timeline data")
	collectCmd.Flags().String(flagConfigOutput, "config.json", "path to write config data")

	_ = collectCmd.MarkFlagFilename(flagConfig, ".yml", ".yaml")
}

func collect(cmd *cobra.Command, _ []string) error {
	cfgPath, _ := cmd.Flags().GetString(flagConfig)
	timelineOutPath, _ := cmd.Flags().GetString(flagTimelineOutput)
	configOutPath, _ := cmd.Flags().GetString(flagConfigOutput)

	data, err := os.ReadFile(cfgPath)
	if err != nil {
		return fmt.Errorf("could not read configuration file %s: %w", cfgPath, err)
	}

	var config cfg.Configuration
	if err := yaml.Unmarshal(data, &config); err != nil {
		return fmt.Errorf("could not read yaml: %w", err)
	}

	// connect to gitlab
	git, err := gitlab.NewClient(os.Getenv(envGitLabToken), gitlab.WithBaseURL(os.Getenv(envGitLabUrl)))
	if err != nil {
		return fmt.Errorf("connecting to gitlab: %w", err)
	}

	incidentService := incidents.NewService(os.Getenv(envGitLabProjectId), git)

	var output []cfg.SiteData
	for _, site := range config.Sites {
		issues, err := incidentService.List(&site, nil)
		if err != nil {
			return err
		}
		smallIssues := make([]cfg.SiteIssue, len(issues))
		for i := range issues {
			smallIssues[i] = cfg.SiteIssue{
				IID:         issues[i].IID,
				State:       issues[i].State,
				Description: issues[i].Description,
				UpdatedAt:   timeOrEmpty(issues[i].UpdatedAt),
				ClosedAt:    timeOrEmpty(issues[i].ClosedAt),
				CreatedAt:   timeOrEmpty(issues[i].CreatedAt),
				Title:       issues[i].Title,
				WebURL:      issues[i].WebURL,
			}
		}
		output = append(output, cfg.SiteData{
			Site:   site,
			Issues: smallIssues,
		})
	}

	f, err := os.Create(timelineOutPath)
	if err != nil {
		return fmt.Errorf("creating %s: %w", timelineOutPath, err)
	}
	defer f.Close()

	if err := json.NewEncoder(f).Encode(&output); err != nil {
		return fmt.Errorf("writing timeline to json: %w", err)
	}

	cf, err := os.Create(configOutPath)
	if err != nil {
		return fmt.Errorf("creating %s: %w", configOutPath, err)
	}
	defer cf.Close()

	if err := json.NewEncoder(cf).Encode(&config); err != nil {
		return fmt.Errorf("writing config to json: %w", err)
	}

	return nil
}

func timeOrEmpty(t *time.Time) int64 {
	if t == nil {
		return 0
	}
	return t.UnixMilli()
}
