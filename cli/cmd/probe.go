package cmd

import (
	"fmt"
	"github.com/djcass44/gl-uptime/cli/internal/cfg"
	"github.com/djcass44/gl-uptime/cli/internal/incidents"
	"github.com/djcass44/gl-uptime/cli/internal/probes"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

var probeCmd = &cobra.Command{
	Use:  "probe",
	RunE: probe,
}

const (
	flagConfig = "config"

	envGitLabToken     = "GITLAB_TOKEN"
	envGitLabUrl       = "GITLAB_URL"
	envGitLabProjectId = "CI_PROJECT_ID"
)

func init() {
	probeCmd.Flags().StringP(flagConfig, "c", ".upptimerc.yml", "path to configuration yaml")

	_ = probeCmd.MarkFlagFilename(flagConfig, ".yml", ".yaml")
}

func probe(cmd *cobra.Command, _ []string) error {
	cfgPath, _ := cmd.Flags().GetString(flagConfig)

	data, err := os.ReadFile(cfgPath)
	if err != nil {
		return fmt.Errorf("could not read configuration file %s: %w", cfgPath, err)
	}

	var config cfg.Configuration
	if err := yaml.Unmarshal(data, &config); err != nil {
		return fmt.Errorf("could not read yaml: %w", err)
	}

	// connect to gitlab
	git, err := gitlab.NewClient(os.Getenv(envGitLabToken), gitlab.WithBaseURL(os.Getenv(envGitLabUrl)))
	if err != nil {
		return fmt.Errorf("connecting to gitlab: %w", err)
	}

	incidentService := incidents.NewService(os.Getenv(envGitLabProjectId), git)

	// iterate through the sites
	log.Printf("checking uptime %s", config.Repo)
	for _, site := range config.Sites {
		ok, resp, err := probes.HttpProbe(cmd.Context(), &site)
		if err != nil {
			return err
		}
		log.Printf("site %s is up: %v", site.Name, ok)
		open, err := incidentService.IsOpen(&site)
		if open && ok {
			// if the request succeeded, close the incident
			if err := incidentService.Close(&site); err != nil {
				return err
			}
		} else if !open && !ok {
			// if the request failed, create an incident
			if err := incidentService.Open(&site, &resp); err != nil {
				return err
			}
		}
	}

	return nil
}
