package cmd

import (
	"github.com/spf13/cobra"
	"os"
)

var command = &cobra.Command{
	Use:          "uptime",
	SilenceUsage: true,
}

func init() {
	command.AddCommand(probeCmd, collectCmd)
}

func Execute(version string) {
	command.Version = version
	if err := command.Execute(); err != nil {
		os.Exit(1)
	}
}
